#!/usr/bin/env python3
# SPDX-License-Identifier: LGPL-2.1-or-later
# I won't say this is well written code, but it works
# I'll fix it if needed, but for now, quick'n'dirty is good enough

import socket, ssl
import datetime, re, random, time
import base64
import requests, json, io, csv
import traceback, signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

sock = None

def print_time(*args):
	print(datetime.datetime.now().strftime("[%H:%M:%S]"), *args, flush=True)

def send(line, log=True):
	if log:
		print_time("<<", line)
	else:
		print_time("<< (redacted)")
	assert '\n' not in line
	sock.send(line.encode("utf-8")+b"\n")

watched_repos = { 5:"wine", 145:"vkd3d", 231:"wine-staging", 1840:"wine-mono" }
next_commits_check = time.time()
latest_commits = [ None ] * len(watched_repos)
merge_request_count = 0
main_channel = "#winehackers"
# main_channel = "##walrus"

line_bytes = b""
ping_sent = False
connected = False

c_bold = "\x02"
c_reset = "\x0F"
c_white = "\x0300"  # use with caution, it looks bad on white background
c_black = "\x0301"  # use with caution, it looks bad on black background
c_darkblue = "\x0302"
c_darkgreen = "\x0303"
c_red = "\x0304"
c_darkred = "\x0305"
c_purple = "\x0306"
c_orange = "\x0307"
c_yellow = "\x0308"  # use with caution, it looks bad on white background
c_green = "\x0309"
c_darkteal = "\x0310"
c_lightteal = "\x0311"
c_blue = "\x0312"  # use with caution, it looks bad on white background
c_pink = "\x0313"  # use with caution, it looks bad on white background
c_gray = "\x0314"
c_lightgray = "\x0315"  # use with caution, it looks bad on white background

def get(url):
	try:
		return requests.get(url)
	except requests.exceptions.ConnectionError:
		class dummy:
			status_code = -1
			text = ""
		return dummy()

def handle_message(text):
	if m := re.search(r"\b[0-9a-fA-F]{8,}\b", text):
		commit = m[0]
		for proj in watched_repos:
			rsp = get("https://gitlab.winehq.org/api/v4/projects/"+str(proj)+"/repository/commits/"+commit)
			if rsp.status_code == 200:
				commit = json.loads(rsp.text)
				return commit["author_name"]+" * "+commit["short_id"]+" : "+commit["title"]
	
	if m := re.search(r"\b[Bb]ug ([0-9]+)\b", text):
		bug_id = m[1]
		text = get("https://bugs.winehq.org/buglist.cgi?columnlist=short_desc,bug_status,resolution&ctype=csv&bug_id="+bug_id).text
		bugs = list(csv.DictReader(io.StringIO(text)))
		if len(bugs):
			bug, = bugs
			status = bug["bug_status"] + bug["resolution"] * (bug["resolution"]!=" ---")
			return c_purple+"Bug "+bug["bug_id"]+c_reset+": ["+c_darkteal+status+c_reset+"] "+bug["short_desc"]+\
				" - "+c_gray+"https://bugs.winehq.org/show_bug.cgi?id="+bug["bug_id"]
		# if not, probably not a valid bug id; just ignore it
	
	if m := re.search(r"!([0-9]+)\b", text):
		mr_id = m[1]
		rsp = get("https://gitlab.winehq.org/api/v4/projects/5/merge_requests/"+mr_id)
		if rsp.status_code == 200:
			# IID is the only one that matters
			# ID is global across the gitlab instance I think; it shows up nowhere
			mr = json.loads(rsp.text)
			return "MR !"+str(mr["iid"])+": ["+mr["state"]+"] "+mr["title"]+\
				" - https://gitlab.winehq.org/wine/wine/-/merge_requests/"+str(mr["iid"])

def check_for_new_commits():
	global merge_request_count
	# needs a big delta, Julliard seems to have a delay between creating commits and pushing to GitLab before the semiweekly release
	# (and there's also some timezone issues in the GitLab API)
	# but the limit needs to exist, to avoid trouble if the latest commit stops existing due to force push
	commits_after = (datetime.datetime.now(tz=datetime.timezone.utc) - datetime.timedelta(hours=24)).isoformat()
	stop = False
	for idx,proj in enumerate(watched_repos):
		url = "https://gitlab.winehq.org/api/v4/projects/"+str(proj)+"/repository/commits?per_page=50&since="+commits_after
		print(url)
		# can throw requests.exceptions.ConnectionError (containing urllib3.exceptions.MaxRetryError,
		#  containing urllib3.exceptions.NewConnectionError, containing ConnectionRefusedError) if server is down
		rsp = get(url)
		if rsp.status_code != 200:
			continue
		commits = json.loads(rsp.text)
		print(commits)
		seen_those_already = any(commit["id"]==latest_commits[idx] for commit in commits)
		if latest_commits[idx]:
			for commit in commits[::-1]:
				if seen_those_already:
					if commit["id"] == latest_commits[idx]:
						seen_those_already = False
					continue
				
				notice = commit["author_name"]+" * "+commit["short_id"]+" : "+commit["title"]
				notice = "["+watched_repos[proj]+"] "+notice
				send("PRIVMSG "+main_channel+" :"+notice)
				time.sleep(2)  # I'd replace this with https://ircv3.net/specs/extensions/multiline if it was implemented on Libera
				stop = True
		if commits:
			latest_commits[idx] = commits[0]["id"]
	if stop:
		# so it doesn't print MRs right beside a wall of text
		return
	
	url = "https://gitlab.winehq.org/api/v4/merge_requests?scope=all&per_page=5"
	print(url)
	rsp = get(url)
	if rsp.status_code == 200:
		mrs = json.loads(rsp.text)
		print(mrs)
		if merge_request_count:
			for mr in mrs[::-1]:
				if mr["id"] <= merge_request_count:
					continue
				if mr["project_id"] not in watched_repos:
					continue
				notice = "New MR: ["+watched_repos[mr["project_id"]]+"] "+mr["title"]+" "+mr["web_url"]
				send("PRIVMSG "+main_channel+" :"+notice)
				time.sleep(2)
		merge_request_count = mrs[0]["id"]
check_for_new_commits()

while True:
	try:
		if sock is None:
			print_time("** Connecting")
			
			sock_raw = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			sock_raw.settimeout(90)
			sock = ssl.create_default_context().wrap_socket(sock_raw, server_hostname="irc.libera.chat")
			sock.settimeout(90)
			sock.connect(("irc.libera.chat",6697))
			print_time("** Connected")
			ping_sent = False
			connected = False
			send("CAP REQ :sasl")
			send("AUTHENTICATE PLAIN")
			passwd = open("passwd.txt","rt").read()
			send("AUTHENTICATE "+base64.b64encode(('winebot\0winebot\0'+passwd).encode("utf-8")).decode("utf-8"), log=False)
			send("CAP END")
			send("USER winebot a a :Sir_Walrus proudly presents: winebot <https://gitlab.winehq.org/Alcaro/winebot>")
			send("NICK winebot")
			send("MODE winebot +B")  # not implemented on the Libera server, but...
		
		try:
			seg = sock.recv(4096)
			if seg == b'':
				print_time("Disconnected: EOF from server")
				sock = None
				continue
			line_bytes += seg
		except TimeoutError:
			send("PING :1")
			if ping_sent:
				print_time("Disconnected: Ping timeout")
				sock = None
				continue
			ping_sent = True
		lines = line_bytes.split(b"\n")
		line_bytes = lines[-1]
		for line in lines[:-1]:
			ping_sent = False
			line = line.decode("utf-8", errors="replace")
			print_time(">>", line)
			
			src_full = ""
			if line.startswith(":"):
				src_full,line = line.split(" ",1)
				src_full = src_full[1:]
			words = []
			while True:
				if line.startswith(":"):
					words.append(line[1:])
					break
				elif ' ' in line:
					word,line = line.split(" ",1)
					words.append(word)
				else:
					words.append(line)
					break
			
			if words[0] == "PING":
				send("PONG :"+words[1])
			if words[0] == "001":
				send("JOIN "+main_channel)
				connected = True
			if words[0] == "433":
				newnick = "winebot"+f'{random.randint(0,9999):04d}'
				send("NICK "+newnick)
				send("MODE "+newnick+" +B")
			if words[0] == "PRIVMSG":
				dst = words[1]
				src = src_full.split("!")[0]
				if not dst.startswith("#"):
					dst = src
				
				reply = handle_message(words[2])
				if reply:
					send("PRIVMSG "+dst+" :"+reply)
		
		if connected and time.time() > next_commits_check:
			check_for_new_commits()
			next_commits_check = time.time()+1800
	
	except Exception:
		traceback.print_exc()
		try:
			send("QUIT :ERROR: bot crashed. I'll be back in a minute")
			time.sleep(3)
			sock.close()
		except Exception:
			pass
		sock = None
		time.sleep(60)
